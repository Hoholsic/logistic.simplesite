const anchors = document.querySelectorAll('a.scroll-to')

for (let anchor of anchors) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault()
    
    const blockID = anchor.getAttribute('href')
    
    document.querySelector(blockID).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  })
}

var menuBtn = document.getElementById('menuBtn');
var menuContainer = document.getElementById('menuContainer');

var menuIconClosed = "menu-icon closed"; 
var menuIconOpened = "menu-icon opened"; 
var menuContClosed = "menu-container closed"; 
var menuContOpened = "menu-container opened"; 

menuBtn.onclick = function() {
	if (menuBtn.className == menuIconClosed) {
	    menuBtn.className = menuIconOpened;
	    menuContainer.className = menuContOpened;
	} else if (menuBtn.className == menuIconOpened) {
	    menuBtn.className = menuIconClosed;
	    menuContainer.className = menuContClosed;
	   }
	}

function change(el){
	var list = document.getElementsByClassName("link");
	console.log(list[0]);
	for (var i = 0; i < list.length; i++) {
		list[i].classList.remove("active");
	}
	el.closest(".link").classList.add("active");
	console.log(document.getElementsByClassName("link"));
}
